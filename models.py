from torch import nn
import torch.nn.functional as F


class BaselineRegressor(nn.Module):
    '''
    Baseline Reggressor architecture
    input [13] -> [13] -> [1] output
    '''

    def __init__(self):
        super(BaselineRegressor, self).__init__()
        self.layer1 = nn.Linear(13, 13)
        self.layer2 = nn.Linear(13, 1)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = self.layer2(x)
        return x


class DeeperRegressor(nn.Module):
    '''
    Deeper Reggressor architecture
    input [13] -> [13] -> [6] -> [1] output
    '''

    def __init__(self):
        super(DeeperRegressor, self).__init__()
        self.layer1 = nn.Linear(13, 13)
        self.layer2 = nn.Linear(13, 6)
        self.layer3 = nn.Linear(6, 1)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        x = self.layer3(x)
        return x


class DeeperRegressor(nn.Module):
    '''
    Deeper Reggressor architecture
    input [13] -> [30] -> [1] output
    '''

    def __init__(self):
        super(WiderRegressor, self).__init__()

    def forward(self, x):
        return x


class DropoutRegressor(nn.Module):
    '''
    Dropout Reggressor architecture
    This net is much bigger then the others to make dropout useful.
    You usually put dropout before last layer. 
    input [13] -> [30] -> [40] -> Dropout -> [1] output
    '''

    def __init__(self, dropout_prob=0.1):
        '''
        :param dropout_prob: Probability that activation of a neuron
                             will be dropped
        '''
        super(DropoutRegressor, self).__init__()
        self.layer1 = nn.Linear(13, 30)
        self.layer2 = nn.Linear(30, 40)
        self.dropout = nn.Dropout(p=dropout_prob)
        self.last = nn.Linear(40, 1)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        x = self.dropout(x)
        x = self.last(x)
        return x
