from tqdm import tqdm
from sklearn.model_selection import ParameterGrid

from models import BaselineRegressor, DeeperRegressor, DropoutRegressor
from train import train
from utils import seed_everything


def train_model_exmple():
    seed_everything()
    model = DropoutRegressor(dropout_prob=0.2)
    model, loss = train(
        model,
        batch_size=15,
        learning_rate=1e-3,
        weight_decay=1e-3,
    )

    print(loss)
    return model, loss


def grid_search_example():
    seed_everything()
    param_grid = {'learning_rate': [3e-3, 1e-3, 3e-4],
                  'batch_size': [5, 10, 15],
                  'weight_decay': [1e-2, 3e-3, 1e-3],
                  'dropout_prob': [0.1, 0.15, 0.2]
                  }
    results = []
    for params in tqdm(ParameterGrid(param_grid)):
        model = DropoutRegressor(dropout_prob=params['dropout_prob'])
        model, loss = train(
            model,
            batch_size=params['batch_size'],
            learning_rate=params['learning_rate'],
            weight_decay=params['weight_decay'],
        )
        results.append((loss, model, params))

    results.sort()
    return results


if __name__ == '__main__':
    # results = grid_search_example()
    # for r in results:
    #     print(r[0], r[2])
    train_model_exmple()
