import torch

from torch import nn
import torch.optim as optim
from torch.utils.data import DataLoader

from utils import load_data


def train(
    model,
    batch_size=32,
    learning_rate=3e-3,
    weight_decay=1e-3,
    max_overfit=5,
):
    '''
    This function trains the model on boston housing dataset.
    At each epoch it trains model on train set and then evaluates if on test set.
    Function implements early stopping, when model overfits training shutdowns.

    :param model: Model instance to be trained
    :param batch_size: Size of batch
    :param learning_rate: Learning rate for optimizer (Adam)
    :param weight_decay: L2 penalty for model training
    :param max_overfit: Maximum number of epochs without loss decrease
    :return: (best model, loss of best model)
    '''
    train_set, val_set = load_data()

    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True)
    test_loader = DataLoader(val_set, batch_size=batch_size)

    optimizer = optim.Adam(model.parameters(),
                           lr=learning_rate,
                           weight_decay=weight_decay
                           )
    criterion = nn.MSELoss()

    best_model_loss = 1e+10
    epoch = 0
    overfit = 0
    best_model = model
    while True:
        epoch += 1

        # train loop
        model.train()
        for x, y in train_loader:
            model.zero_grad()
            y = y.view(-1, 1)
            pred = model(x)
            loss = criterion(pred, y)
            loss.backward()
            optimizer.step()

        # evaluation loop
        loss_sum = 0
        model.eval()
        with torch.no_grad():
            for x, y in test_loader:
                y = y.view(-1, 1)
                pred = model(x)
                loss = criterion(pred, y)
                loss_sum += loss.item()

        loss_sum /= (len(val_set) / batch_size)
        # print(f"Epoch: {epoch} loss: {loss_sum}")
        if best_model_loss > loss_sum:
            best_model_loss = loss_sum
            best_model = model
            overfit = 0
        else:
            overfit += 1

        if overfit > max_overfit:
            break

    return best_model, best_model_loss
